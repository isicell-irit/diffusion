#ifndef ONKO3D_3_0_DIFFUSIONGRID_HPP
#define ONKO3D_3_0_DIFFUSIONGRID_HPP

/**
 * @file DiffusionGrid.hpp
 * @brief Defines the DiffusionGrid class for molecule diffusion behavior.
 *
 * This file contains the DiffusionGrid class used in the diffusion plugin.
 */

#include <vector>
#include <typeindex>
#include <typeinfo>
#include <unordered_map>
#include <mecacell/utilities/utils.hpp>

/**
 * @namespace Diffusion
 * @brief Namespace for diffusion-related structures and classes.
 */
namespace Diffusion {

    /**
     * @struct Molecule
     * @brief Structure representing a molecule in the diffusion grid.
     */
    struct Molecule {
        static constexpr double spheroidDensity = 0.001; /**< Density of the spheroid in ng/µm³ */
        static constexpr double henryConst = 22779000000.; /**< Henry constant for oxygen at human body temperature in µm³.mmHg/ng */
        double diffusion; /**< Diffusion constant in µm²/s */
        double defaultQuantity; /**< Default quantity of the molecule in the environment without cells in mmHg */
        double omega; /**< Henry constant for the molecule at human temperature multiplied by density of a tumor and density of the molecule in mmHg.ng/µm³ */
        double defaultEvaporation; /**< Consumption constant of the environment in µm³/(ng.s) */

        /**
         * @brief Constructor to initialize a molecule with given parameters.
         * @param d Diffusion constant
         * @param dQ Default quantity
         * @param density Density of the molecule
         * @param dE Default evaporation rate
         */
        inline Molecule(double d, double dQ, double density, double dE) : diffusion(d), defaultQuantity(dQ),
        omega(spheroidDensity  * density * henryConst), defaultEvaporation(dE) {}
    };

    /**
     * @struct GridCell
     * @brief Structure representing a cell in the diffusion grid.
     */
    struct GridCell {
        vector<double> prevQuantities; /**< Previous quantities of each molecule in mmHg */
        vector<double> quantities; /**< Current quantities of each molecule in mmHg */
        vector<double> consumptions; /**< Local consumptions of each molecule in µm³/(ng.s) */

        /**
         * @brief Default constructor.
         */
        inline GridCell() = default;

        /**
         * @brief Constructor to initialize a grid cell with given molecules.
         * @param m Existing molecules in the simulation
         */
        explicit GridCell(vector<Molecule> m) {
            int size = m.size();
            quantities.resize(size);
            prevQuantities.resize(size);
            consumptions.assign(size, 0.);
            for (int i = 0; i < size; ++i) {
                quantities[i] = m[i].defaultQuantity;
                prevQuantities[i] = m[i].defaultQuantity;
            }
        }
    };

    /**
     * @class DiffusionGrid
     * @brief Class representing a grid for molecule diffusion.
     */
    class DiffusionGrid {
    private:
        double dx = 5.; /**< Cell size */
        double accuracy = 0.1; /**< Precision of the diffusion */
        unordered_map<MecaCell::Vec, GridCell> grid; /**< Grid cells hashmap */
        vector<Molecule> molecules; /**< Existing molecules in the simulation */

        // Boundaries
        MecaCell::Vec minBoundary;
        MecaCell::Vec maxBoundary;

        /**
         * @brief Calculates the second derivative.
         * @param xm1 Value at x-1
         * @param x Value at x
         * @param xp1 Value at x+1
         * @return Approximate second derivative at x
         */
        inline double deriv2nd(double xm1, double x, double xp1) { return ((xm1 - (2.0 * x) + xp1) / (dx * dx)); }

        /**
         * @brief Initializes the next step for a molecule.
         * @param i Molecule index
         */
        void nextStep(int i) {
            for (auto &c : grid) {
                c.second.prevQuantities[i] = c.second.quantities[i];
            }
        }

        /**
         * @brief Empties all cells.
         */
        void allCellEmpty() {
            for (auto &c : grid) {
                for (int i = 0; i < molecules.size(); ++i) { // For each molecule
                    c.second.consumptions[i] = 0.;
                }
            }
        }

        /**
         * @brief Cleans cells that are out of boundaries.
         */
        void cleanCellOutBoundary() {
            for (auto &c: grid) {
                if (isOutBoundary(c.first)) {
                    grid.erase(c.first);
                }
            }
        }

        /**
         * @brief Checks if a position is out of boundaries.
         * @param v Position vector
         * @return True if the position is out of boundaries, false otherwise
         */
        bool isOutBoundary(const MecaCell::Vec &v) {
            return(v.x()<minBoundary.x() || v.y()<minBoundary.y() || v.z()<minBoundary.z() || v.x()>maxBoundary.x() || v.y()>maxBoundary.y() || v.z()>maxBoundary.z());
        }

        /**
         * @brief Gets the diffusion time step.
         * @param D Diffusion constant of the molecule
         * @return Diffusion time step
         */
        inline double getDt(double D) const { return (dx * dx) / (6 * D); }

        /**
         * @brief Updates the quantity of a molecule for each cell and returns the total amount of this molecule.
         * @param i Molecule index
         * @return Total quantity of the molecule
         */
        double computeStep(int i) {
            Molecule m = molecules[i]; // Considered molecule
            double D = m.diffusion; // Diffusion constant in µm²/s
            double dt = getDt(D); // Diffusion time step in s
            double total = 0; // Total amount of considered molecule in mmHg
            for (auto &c : grid) { // For each cell in the grid
                GridCell &gc = c.second;
                double prevQuantities = gc.prevQuantities[i];
                // Laplacian in mmHg/µm²
                double laplacian = deriv2nd(getMolecule(c.first - MecaCell::Vec(1, 0, 0), i), prevQuantities,
                                            getMolecule(c.first + MecaCell::Vec(1, 0, 0), i)) +
                                   deriv2nd(getMolecule(c.first - MecaCell::Vec(0, 1, 0), i), prevQuantities,
                                            getMolecule(c.first + MecaCell::Vec(0, 1, 0), i)) +
                                   deriv2nd(getMolecule(c.first - MecaCell::Vec(0, 0, 1), i), prevQuantities,
                                            getMolecule(c.first + MecaCell::Vec(0, 0, 1), i));
                gc.quantities[i] = prevQuantities + (D * laplacian - ((gc.consumptions[i] + m.defaultEvaporation) * m.omega)) * dt;
                if (gc.quantities[i] < 0.) { // Can't have a negative amount of molecule
                    gc.quantities[i] = 0.;
                }
                total += gc.quantities[i];
            }
            return (total);
        }

    public:

        unordered_map<int,int> moleculesDict; /**< Dictionary mapping molecule indices */

        /**
         * @brief Default constructor.
         */
        DiffusionGrid() = default;

        /**
         * @brief Constructor with grid parameters.
         * @param dx Grid cell size
         * @param accuracy Diffusion accuracy
         */
        inline DiffusionGrid(double dx, double accuracy) : dx(dx), accuracy(accuracy), grid(), molecules() {}

        /**
         * @brief Gets the grid cell size.
         * @return Grid cell size
         */
        inline double getDx() const { return dx; }

        /**
         * @brief Sets the grid cell size.
         * @param _dx Grid cell size
         */
        inline void setDx(double _dx) { dx = _dx; }

        /**
         * @brief Sets the diffusion accuracy.
         * @param a Diffusion accuracy
         */
        inline void setAccuracy(double a) { accuracy = a; }

        /**
         * @brief Gets the grid index from a position.
         * @param v Position vector
         * @return Grid index of the position
         */
        MecaCell::Vec getIndexFromPosition(const MecaCell::Vec &v) const {
            MecaCell::Vec res = v / dx;
            return MecaCell::Vec(floor(res.x()), floor(res.y()), floor(res.z()));
        }

        /**
         * @brief Gets the quantity of a molecule at a position.
         * @param v Position vector
         * @param m Molecule index
         * @return Quantity of the molecule at the position
         */
        double getMolecule(MecaCell::Vec v, int m) {
            if (grid.count(v) <= 0)
                return (molecules[m].defaultQuantity);
            else
                return(grid[forward<MecaCell::Vec>(v)].quantities[m]);
        }

        /**
         * @brief Gets the size of the grid.
         * @return Size of the grid
         */
        inline size_t size() const { return grid.size(); }

        /**
         * @brief Gets the grid.
         * @return Grid
         */
        inline unordered_map<MecaCell::Vec, GridCell> &getGrid() { return grid; }

        /**
         * @brief Gets the grid cell size.
         * @return Grid cell size
         */
        inline double getCellSize() const { return dx; }

        /**
         * @brief Gets the molecules in the grid.
         * @return Molecules
         */
        inline vector<Molecule> getMolecules() const { return molecules; }

        /**
         * @brief Adds a molecule to the grid.
         * @param n Molecule index
         * @param m Molecule to be added
         */
        void addMolecule(int n, Molecule m) {
            moleculesDict[n] = (int) molecules.size(); 
            molecules.push_back(m); 
        }

        /**
         * @brief Updates the grid boundaries and consumptions.
         * @tparam world_t Type of the world
         * @param w Pointer to the world
         */
        template<typename world_t>
        void updateBoundary(world_t *w) {
            MecaCell::Vec min(9999,9999,9999);
            MecaCell::Vec max(-9999,-9999,-9999);
            allCellEmpty();
            for (auto &c : w->cells) { // For each cell
                MecaCell::Vec center = getIndexFromPosition(c->getBody().getPosition());

                // Update min and max boundaries
                if(center.x()<min.x()) min.setX(center.x());
                if(center.y()<min.y()) min.setY(center.y());
                if(center.z()<min.z()) min.setZ(center.z());

                if(center.x()>max.x()) max.setX(center.x());
                if(center.y()>max.y()) max.setY(center.y());
                if(center.z()>max.z()) max.setZ(center.z());
                
                // Update consumptions and create new grid cells
                if (grid.count(forward<MecaCell::Vec>(center)) <= 0) {
                    GridCell gc = GridCell(molecules);
                    for (int i = 0; i < molecules.size(); ++i) { // For each molecule
                        gc.consumptions[i] += c->getBody().getConsumptionByIndex(i);
                    }
                    grid[forward<MecaCell::Vec>(center)] = gc;
                } else {
                    for (int i = 0; i < molecules.size(); ++i) { // For each molecule
                        grid[forward<MecaCell::Vec>(center)].consumptions[i] += c->getBody().getConsumptionByIndex(i);
                    }
                }
            }

            minBoundary = min;
            maxBoundary = max;
            for(int x=minBoundary.x();x<maxBoundary.x();x++){
                for(int y=minBoundary.y();y<maxBoundary.y();y++){    
                    for(int z=minBoundary.z();z<maxBoundary.z();z++){
                        MecaCell::Vec pos(x,y,z);
                        if (grid.count(forward<MecaCell::Vec>(pos)) <= 0) {
                            grid[forward<MecaCell::Vec>(pos)] = GridCell(molecules);
                        }
                    }   
                }
            }
        }

        /**
         * @brief Updates the quantities of molecules in the grid.
         * @tparam world_t Type of the world
         * @param w Pointer to the world
         */
        template<typename world_t>
        void computeMolecules(world_t *w) {
            vector<double> lasts; // Last step quantities
            vector<double> news; // Current step quantities
            lasts.assign(molecules.size(),0.);
            news.assign(molecules.size(), INFINITY);
            for (int i = 0; i < molecules.size(); ++i) { // For each molecule
                double t = 0; // Elapsed time
                double dt = getDt(molecules[i].diffusion); // Diffusion time step
                while (abs(lasts[i] - news[i]) / grid.size() >= accuracy && t < w->dt) {
                    lasts[i] = news[i];
                    nextStep(i);
                    news[i] = computeStep(i);
                    t += dt;
                }
            }
        }

        /**
         * @brief Gets the quantity of a molecule at a real position.
         * @param v Position vector
         * @param mol Molecule index
         * @return Quantity of the molecule at the position
         */
        inline double getMoleculeRealPos(const MecaCell::Vec &v, int mol) {
            return (getMolecule(getIndexFromPosition(v), mol));
        }
    };
}

#endif //ONKO3D_3_0_DIFFUSIONGRID_HPP
