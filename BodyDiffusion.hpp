#ifndef ONKO3D_3_0_BODYDIFFUSION_HPP
#define ONKO3D_3_0_BODYDIFFUSION_HPP

/**
 * @file BodyDiffusion.hpp
 * @brief Defines the BodyDiffusion class for molecule diffusion bodies.
 *
 * This file contains the BodyDiffusion class that works with the diffusion plugin.
 */

#include "DiffusionGrid.hpp"
#include "../../../../src/core/BaseBody.hpp"
#include <vector>
#include <hash_map>

using namespace std;

/**
 * @namespace Diffusion
 * @brief Namespace for diffusion-related structures and classes.
 */
namespace Diffusion {

    /**
     * @class BodyDiffusion
     * @brief Template class for a molecule diffusion body.
     * 
     * @tparam cell_t Type of the cell used in the grid
     * @tparam plugin_t Type of the plugin used
     */
    template<typename cell_t, class plugin_t>
    class BodyDiffusion : virtual BaseBody<plugin_t> {

    private:
        vector<double> consumptions; /*!< consumption of each molecules in µm^3/(ng.s)*/
        DiffusionGrid * grid = nullptr;
        int nbMolecules = 0;

    public:

        /**
         * @brief Default constructor.
         */
        inline BodyDiffusion() : consumptions()//, quantities()
        {}

        /**
         * @brief Constructor with size parameter.
         * @param size Number of existing molecules.
         */
        inline explicit BodyDiffusion(int size){
            initNbMolecules(size);
        }

        /**
         * @brief Initializes the number of molecules.
         * @param n Number of existing molecules.
         */
        void initNbMolecules(int n){ 
            if(n > nbMolecules) {
                nbMolecules = n;
                consumptions.assign(n,0.);
            }
        }

        /**
         * @brief Gets the quantity of a molecule.
         * @param v Position vector.
         * @param mol Number of the molecule.
         * @return Quantity of the molecule.
         */
        double getQuantity(MecaCell::Vec v, int mol) const{ 
             if(this->cellPlugin != nullptr) return grid->getMoleculeRealPos(v, grid->moleculesDict[mol]); 
             else return 0.;
        }

        /**
         * @brief Gets the consumption of a molecule.
         * @param mol Number of the molecule.
         * @return Consumption of the molecule.
         */
        double getConsumption(int mol) const{ 
            if(this->cellPlugin != nullptr) return consumptions[grid->moleculesDict[mol]]; 
            else return consumptions[mol];
        }

        /**
         * @brief Gets the consumption of a molecule by index.
         * @param i Index of the molecule.
         * @return Consumption of the molecule.
         */
        double getConsumptionByIndex(int i) const{ 
            return consumptions[i]; 
        }

        /**
         * @brief Sets the consumption of a molecule.
         * @param mol Number of the molecule.
         * @param value Consumption value.
         */
        inline void setConsumption(int mol, double value){ 
            if(this->cellPlugin != nullptr) consumptions[grid->moleculesDict[mol]] = value;
            else consumptions[mol] = value;
        }

        /**
         * @brief Gets the diffusion grid.
         * @return Pointer to the diffusion grid.
         */
        inline DiffusionGrid * getDiffusionGrid(){ return this->cellPlugin->pluginDiffusion.getGrid(); }

        /**
         * @brief Sets the diffusion grid.
         * @param g Pointer to the diffusion grid.
         */
        void setDiffusionGrid(DiffusionGrid * g){
            if(grid == nullptr){
                vector<double> consCopy(consumptions);
                for(int i=0; i<consumptions.size(); ++i) consumptions[i] = consCopy[g->moleculesDict[i]];
            }
            grid = g; 
        }

        /**
         * @brief Hook called when the body has access to the plugin.
         *
         * This happens when the cell is added to the world (w.addCell(c)).
         * This hook can be useful for setting up Body properties based
         * on elements from the Plugin part.
         */
        void onCellPluginLinking(){
            auto grid = getDiffusionGrid();
            vector<double> consCopy(consumptions);
            for(int i=0; i<consumptions.size(); ++i) consumptions[i] = consCopy[grid->moleculesDict[i]];
        }

    };

}


#endif //ONKO3D_3_0_BODYDIFFUSION_HPP
