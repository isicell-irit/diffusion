#ifndef ONKO3D_3_0_PLUGINDIFFUSION_HPP
#define ONKO3D_3_0_PLUGINDIFFUSION_HPP

/**
 * @file PluginDiffusion.hpp
 * @brief Defines the PluginDiffusion class for grid diffusion-based plugins.
 *
 * This file contains the PluginDiffusion class that works with the DiffusionGrid.
 */

#include <vector>
#include <mecacell/mecacell.h>
#include <math.h>
#include "DiffusionGrid.hpp"

/**
 * @namespace Diffusion
 * @brief Namespace for diffusion-related structures and classes.
 */
namespace Diffusion {

    /**
     * @class PluginDiffusion
     * @brief Template class for a grid diffusion-based plugin.
     * 
     * @tparam cell_t Type of the cell used in the grid.
     */
    template<typename cell_t>
    class PluginDiffusion {

    private:
        DiffusionGrid grid; /**< Grid for diffusion calculations */

    public:

        /**
         * @brief Default constructor.
         */
        PluginDiffusion() = default;

        /**
         * @brief Constructor with grid parameters.
         * @param dx Grid cell size
         * @param accuracy Diffusion accuracy
         */
        inline PluginDiffusion(double dx, double accuracy): grid(dx, accuracy) {}

        /**
         * @brief Sets the grid cell size.
         * @param dx Grid cell size
         */
        inline void setDx(double dx) { grid.setDx(dx); }

        /**
         * @brief Sets the diffusion accuracy.
         * @param a Diffusion accuracy
         */
        inline void setAccuracy(double a) { grid.setAccuracy(a); }

        /**
         * @brief Gets the diffusion grid.
         * @return Pointer to the diffusion grid
         */
        inline DiffusionGrid* getGrid() { return &grid; }

        /**
         * @brief Adds a new molecule to the grid.
         * @param n Molecule index
         * @param m Molecule to be added
         */
        inline void addMolecule(int n, Molecule m) { grid.addMolecule(n, m); }

        /**
         * @brief Hook called when a cell is added to the world.
         * @tparam world_t Type of the world
         * @param w Pointer to the world
         */
        template<typename world_t>
        void onAddCell(world_t *w) {
            for (cell_t *c : w->newCells){
                c->getBody().setDiffusionGrid(&grid); // Transmits the diffusion grid
            }
        }

        /**
         * @brief Hook called before the behavior update of the world.
         * 
         * Computes and updates the molecule quantities for each cell.
         * 
         * @tparam world_t Type of the world
         * @param w Pointer to the world
         */
        template<typename world_t>
        void preBehaviorUpdate(world_t *w) {
            grid.updateBoundary(w);
            grid.computeMolecules(w);
        }
    };
}

#endif //ONKO3D_3_0_PLUGINDIFFUSION_HPP